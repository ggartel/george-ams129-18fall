#setup.py

import os
import numpy as np
import matplotlib.pyplot as plt


#init main file 
if __name__ == "__main__":

    print("It's a SETUP ")

    #Compiles your code using the makefile (in ../fortran)
    os.chdir('../fortran') # Enters fortran
    os.system('make')

    #Run your code,
    os.system('./euler')

    os.chdir('../python') # Returns to python folder

    #Load output_i.txt file as a Numpy array, for i = 8,16,32,64     

    #list
    i = [8,16,32,64]

    # loop i length
    j = 0

    while j < len(i):
    
        # open txt file output_i.txt   
    
        path = os.path.dirname(os.path.realpath('__file__'))
    
        fn = os.path.join(path, '../fortran/output_%d.txt' %i[j])
    
        data = os.path.abspath(os.path.realpath(fn))

        #Load output_i.txt file as a Numpy array, for i = 8,16,32,64
        nSol = np.loadtxt(data)


    
       #Calculate the Error
        t = []
        rSol = []
        x = 0
        error = 0.
        while(x < len(nSol)):
            #Calculate Real Soln
            t.append(nSol[x][0])
            rSol.append(-1*np.sqrt(2*np.log((t[x]**2)+1)+4))

            #Calculate Total Error
            error += abs(rSol[x] - nSol[x][1])  
            x += 1
        
    # produce a figure called result_i.png for i = 8,16,32,64
############################## PLOT ##############################

        a,b = nSol.T
        #plt.interactive(False)
        fig = plt.figure()
#Plot the numerical result with red dashed line with circled markers
        plt.plot(a,b, linestyle="--", marker='o', color = "red")

#Plot the real solution on the same figure with blue solid line
        plt.plot(a,rSol, linestyle="-", color = "blue")

#Set a title of the figure as the calculated error in step d.
        plt.title(error)

#Set t- and y-labels as “t axis” and “y axis”.
        plt.xlabel("t axis")
        plt.ylabel("y axis")

#Set a grid.
        plt.grid(True)
#Your Python code must produce 4 figure files (result_i.png for i = 8,16,32,64) in a single run.
        fig.savefig("results_%d.png" %i[j])
        j += 1
    os.chdir('../fortran') # Enters fortran
    os.system('make clean') # clean fortran folder after exe
    os.chdir('../python') # Returns to python folder


