#main.py

import newton as n

#A function f(x) which defines the equation (14).
def f(x):
    
    return 2*x**7+4*x**5-2*x**3+3*x+1

#A function df(x) which defines a derivative of the equation (14).
def df(x):
    return 14*x**6+20*x**4-6*x**2+3

######################## MAIN BLOCK ############################# 
if __name__ == "__main__":

    initial_guess =10.
    threshold = 1.e-8

    n.newtons(f = f, df = df, initial_guess = initial_guess, threshold = threshold)
