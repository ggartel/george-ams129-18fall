#newton.py

#func newtons(f, df, initial_guess, threshold)
def newtons(f, df, initial_guess, threshold):
# calc the root newtons method

    i = 0
    dx = initial_guess
    xnew = initial_guess

    print("  n|                     x|                    dx")
    print("--------------------------------------------------")
    print("{ii}|{xx}|{ddxx}".format(ii = str(i).rjust(3), xx = "10.0".rjust(22), ddxx = "10.0".rjust(22)))
    
    while(dx > threshold):
        xold = xnew
        i += 1
        
        xnew = xold - (f(xold)/df(xold))
        
        dx = abs(xold - xnew)
        print("{ii}|{xx}|{ddxx}".format(ii = str(i).rjust(3), xx = str(xnew).rjust(22), ddxx = str(dx).rjust(22)))
      
