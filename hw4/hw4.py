# Homework 4
#George Gartel

import numpy as np


###################### PART 1 ####################################

def right_justify(str):

    s = str
    print(s.rjust(70))

    return;

###################### PART 2 ####################################

def count_char(str,chr): 

    s = str
    c = chr.lower()
    

    if (len(s) > 0):
        
        cnt = s.lower().count(c)
        print("Total number of ", c, " in the given string: ", cnt)
 
    return;

####################### PART 3 ####################################

def cumulative_sum(list):
    
    print(list)
    i = np.cumsum(list)

    return(i);

######################## PART 4 ###################################

def check_palindrome(str):
    
    ln = len(str)
    if(ln < 2):
        return True
    elif(str[0] != str[-1]):
        return False
    else:
        return check_palindrome(str[1:ln-1])

###################################################################
#
#
#		MAIN BLOCK OF CODE
#
###################################################################
if __name__ == "__main__":

    right_justify("George Gartel")
    print(' ')

    lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis tellus id mollis scelerisque. In consequat nec tellus lacinia iaculis. Mauris auctor volutpat aliquam. Nulla dignissim arcu placerat tellus pretium, vel venenatis sem porta. Donec interdum tincidunt mi, et convallis velit aliquet eget. Nam id rutrum felis. Fusce vel fermentum justo. Pellentesque faucibus orci at velit vehicula dignissim. Duis faucibus dapibus malesuada. Pellentesque iaculis tristique vestibulum. Sed egestas nisl non augue imperdiet mattis. Nunc vitae purus lectus. Vestibulum mi turpis, volutpat vel odio quis, hendrerit suscipit ligula. Nunc in massa diam. Suspendisse aliquam quam et ex egestas vestibulum vitae id libero. Cras molestie consectetur condimentum.'

    count_char(lorem, 'T')
    print(' ')

    result = cumulative_sum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    print(result)
    print(' ')

    words_list = ['noon', 'madam', 'ams129', 'redivider', 'numpy', 'bob', 'racecar', 'youngjun']
    for word in words_list:
        if(check_palindrome(word)):
            print(word, 'is palindrome!')
            print(' ')
