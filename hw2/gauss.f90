!future

program gauss

  implicit none

  real(kind=8), dimension(3,3) :: A
  real(kind=8), dimension(3) :: b

  integer :: i,j

  ! initialize matrix A and vector b
  A(1,:) = (/2., 3., -1./)
  A(2,:) = (/4., 7., 1./)
  A(3,:) = (/7., 10., -4./)

  b = (/1., 3., 4./)

  open (unit=21, file="soln.txt") !write to created soln text file
  

! print augmented matrix
  do i = 1, 3           ! i is row
    write(21,*) A(i,:), "|", b(i)
    print *, A(i,:), "|", b(i)
  end do

  print *, ""    ! print a blank line
  write(21,*) ""
  print *, "Gaussian elimination........"
  write(21,*)  "Gaussian elimination........"

  ! gaussian elimination
  do j = 1, 2           ! j is column
    do i = j+1, 3       ! i is row

	b(i)= b(i)-(A(i,j)/A(j,j))*(b(j)) ! Gauss Vector Elimination	
	A(i,:) = A(i,:)-(A(i,j)/A(j,j))*(A(j,:)) !Gauss Matrix Elimination
	
    end do
  end do


  ! print augmented matrix again
  ! this should be an echelon form (or triangular form)
  
  print *, "***********************"
  do i = 1, 3
    print *, A(i,:), "|", b(i)
    write(21,*) A(i,:), "|", b(i)
  end do

  print *, ""    ! print a blank line
  write(21, *) ""
  print *, "back subs......"
  write(21,*) "back subs......"

  ! doing back substitution
  do j = 3,1,-1            ! j is column
    do i = j-1,1,-1 ! FINISH THIS LOOP        ! i is row
		
	b(i)= b(i)-(A(i,j)/A(j,j))*(b(j)) ! Gauss Vector Elimination	
	A(i,:) = A(i,:)-(A(i,j)/A(j,j))*(A(j,:)) !Gauss Matrix Elimination

    end do
  end do

  !print the results
  print *, "***********************"
  write(21, *) "***********************"

  do i = 1, 3
    print *, A(i,:), "|", b(i)
    write(21, *) A(i,:), "|", b(i)

  end do

  do i = 1,3
	
    b(i)=b(i)/A(i,i)
    A(i,i)=A(i,i)/A(i,i) 

    !b(i)= b(i)-(A(i,j)/A(j,j))*(b(j)) ! Gauss Vector Elimination	
    !A(i,:) = A(i,:)-(A(i,j)/A(j,j))*(A(j,:)) !Gauss Matrix Elimination
	
  end do

  print *, "The solutions are:"
  write(21, *) "The solutions are:"
  
  !print the results
  print *, "x = ", b(1)/A(1,1), ", y = ", b(2)/A(2,2), ", z = ", b(3)/A(3,3) 
  write(21,*) "x = ", b(1)/A(1,1), ", y = ", b(2)/A(2,2), ", z = ", b(3)/A(3,3)

  close(21)

end program gauss
