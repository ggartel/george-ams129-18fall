# plotter.py file

import numpy as np
import matplotlib.pyplot as plt
import collections

def word_count(data):

    raw_data = open(data)

    new_data = raw_data.read()

    clean_data = new_data.replace("\n", "")

    d = dict()

    for c in clean_data:
        if c not in d:
            d[c] = 1
        else:
            d[c] += 1
    od = collections.OrderedDict(sorted(d.items()))

    return(od)

    ######################## MAIN FILE ##########################
if __name__ == "__main__":

    file_path = "./words.txt"

    h = word_count(file_path)

    # make a LIST which contains KEY of hist (d)
    lst = list(h.keys())

    #make a NUMPY ARRAY contains each Value of hist (d)
    narray = np.array(list(h.values()))
      
    x = lst #x-axis 
    y = narray #y-axis
    plt.bar(x,y) # plt graph
    plt.plot(x,y, '-o', color = "red") #plt line

    plt.interactive(False)
    plt.xlabel("Character")
    plt.ylabel("Frequency")
    plt.grid()
    
    plt.savefig("result.png")

    plt.show()

